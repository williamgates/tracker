from django.apps import AppConfig

class ObjectTrackerConfig(AppConfig):
    name = 'object_tracker'
