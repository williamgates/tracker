from django.db import models

class Position(models.Model):
    latitude = models.FloatField(default=0.0)
    longitude = models.FloatField(default=0.0)

class Device(models.Model):
    device_id = models.CharField(max_length=20)
    position = models.ForeignKey(Position, on_delete=models.CASCADE)

class User(models.Model):
    device = models.ForeignKey(Device, on_delete=models.CASCADE)
    username = models.CharField(max_length=20)
    password = models.CharField(max_length=20)
