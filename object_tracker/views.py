from django.http import Http404, HttpResponse, JsonResponse
from django.shortcuts import render
from .models import Device, Position, User

def index(request):
    return render(request, 'object_tracker/index.html', {})

def track(request):
    try:
        user = User.objects.get(username=request.POST['username'])
        device_id = user.device.device_id
    except:
        raise Http404('Error')
    return render(request, 'object_tracker/track.html', {'device_id': device_id})

def sign_in(request):
    return render(request, 'object_tracker/sign_in.html', {})

def get_position(request):
    try:
        device = Device.objects.get(device_id=request.GET['device_id'])
        latitude = device.position.latitude
        longitude = device.position.longitude
    except:
        raise Http404('Error')
    return JsonResponse({'latitude': latitude, 'longitude': longitude})

def update_position(request):
    try:
        device = Device.objects.get(device_id=request.GET['device_id'])
        device.position.latitude = request.GET['latitude']
        device.position.longitude = request.GET['longitude']
        device.position.save()
    except:
        raise Http404('Error')
    return HttpResponse('Success')
