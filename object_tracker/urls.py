from . import views
from django.urls import path

urlpatterns = [
    path('', views.index, name='index'),
    path('sign-in/', views.sign_in, name='sign_in'),
    path('track/', views.track, name='track'),
    path('get-position/', views.get_position, name='get_position'),
    path('update-position/', views.update_position, name='update_position'),
]
