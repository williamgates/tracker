from django.contrib import admin
from .models import Device, Position, User

admin.site.register(Device)
admin.site.register(Position)
admin.site.register(User)
