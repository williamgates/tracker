# Tracker

## Sample Usage

```python
import requests

def get_position(device_id):
    url = 'https://perantau.herokuapp.com/get-position/'
    params = {
        'device_id': device_id
    }

    try:
        init_res = requests.get(url, params=params)
        print(init_res.status_code)
        print(init_res.json())
    except:
        print('Error')

def update_position(device_id, latitude, longitude):
    url = 'https://perantau.herokuapp.com/update-position/'
    params = {
        'device_id': device_id,
        'latitude': latitude,
        'longitude': longitude
    }

    try:
        init_res = requests.get(url, params=params)
        print(init_res.status_code)
    except:
        print('Error')

if __name__ == "__main__":
    device_id = 'z67TxzpCDW'
    update_position(device_id, -6.364552, 106.828770)
```
